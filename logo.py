#!/usr/bin/env python3
from arthax import *
import sys

def fprint(text, font=DEFAULT_FONT, chr_ignore=True, handle=None):
    if isinstance(text, str) is False:
        raise artError("text should have str type")
    split_list = text.split("\n")
    result = ""
    for item in split_list:
        if len(item) != 0:
            result = result + text2art(item, font=font, chr_ignore=chr_ignore)
    print(result, file=handle)

def gen(text: str):
    with open("titles.txt",'w') as handle:
        for item in sorted(list(FONT_MAP.keys())):
                print(str(item) + " : ", file=handle)
                if str(item) in ["char4", "c2", "war_of_w", "coil_cop", "fbr12"]:
                    fprint(text.upper(), str(item), True, handle)
                else:
                    fprint(text, str(item), True, handle)

gen(sys.argv[1])
